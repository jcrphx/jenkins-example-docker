# jenkins-example-docker

# Overview

This demo has been created in response to two challenging tasks for software engineers interested in DevOps positions. The tasks are designed to 
measure abilities in different technologies and concepts such as:

* Software Development in general by creating an extremely minimal service
* Docker Container
* Kubernetes in particular from AWS cloud native flavor of kubernetes (EKS)
* Terraform writing modules and using these for deploy infrastructure 
* Infrastructure as a code 

# Toolset:
* OS platform:  Windows 10 pro supporting Windows Subsystem for Linux (WSL2) running Ubuntu v20.04 
* Python v3.10.1
	* Flask module 2.0.2 
* Docker engine v20.10.11
* Minikube v1.22.0
    * kubectl v1.21.2
* Terraform v1.1.0
* Docker Hub (jcrphx0330/simpletimeservice:v1.0.0)

# Task 1 - Minimalist Application Development / Docker / Kubernetes
## Tiny App Development: 'SimpleTimeService'

- Create a simple microservice (which we will call "SimpleTimeService") in any programming language of your choice: NodeJS, Python, C#, Ruby, whatever you like.  
- The application should be a web server that returns a pure JSON response with the following structure, when its `/` URL path is accessed:

```json
{
  "timestamp": "<current date and time>",
  "ip": "<the IP address of the visitor>"
}
```

Python application: 
```json
/simpletimeservice/app/app.py
```

## Dockerize SimpleTimeService

- Create a Dockerfile for this microservice.
- Your application must be configured to run as a non-root user in the container.
- Publish the image to a public DockerHub repo so we can pull it for testing.

Dockerfile file: 
```sh
/simpletimeservice/app/Dockerfile
```

## Create a k8s manifest for SimpleTimeService

- Create a Kubernetes manifest in YAML format, containing a Deployment and a Service, to deploy your microservice on Kubernetes. 
- Your Deployment must use your public Docker image from DockerHub.

Kubernetes manifest file: 
```sh
/simpletimeservice/app/k8/simpletimeservice.yml
```
## Push your code to a public git repo

## Acceptance Criteria

Your task will be considered successful if a colleague is able to deploy your manifests to a running Kubernetes cluster and use your microservice.   We will use Docker Desktop to test this exercise.

Assuming that your manifest file is named `microservice.yml`, the command:


```sh
kubectl apply -f microservice.yml # i.e. your manifest file
```

 ToDo:
 
1. Starting minikube (In windows 10 pro). Powershell terminal  
```sh
 minikube start --driver=hyperv 

```

2. Verifying minikube is ready 
```sh
  kubectl get nodes
  output:
 NAME       STATUS   ROLES                  AGE   VERSION
minikube   Ready    control-plane,master   53s   v1.21.2
```

3. Clone repository to your local area 
```sh
   mkdir workspace
   cd workspace 
   git clone https://jcrphx@bitbucket.org/jcrphx/simpletimeservice.git
   cd simpletimeservice/app/k8
```

4. Deploying "simpletimeservice.yml" manifest to minikube
```sh
 kubectl apply -f .\simpletimeservice.yml
 
 output:
 service/simple-time-service created
 deployment.apps/simple-time-py created
 
```

5. Verifying the pods are running 
```sh
   kubectl get pods
   output:
   NAME                              READY   STATUS    RESTARTS   AGE
simple-time-py-67ddb46b95-hmzdq   1/1     Running   0          41s
simple-time-py-67ddb46b95-nz6tg   1/1     Running   0          41s
```

6. Validating the Flask Web server is working as expected
Notes: Copy the URL in the browser you suppose received the response message including the "timestamp" and "visitor IP" in JSON format  
```sh
   minikube service simple-time-service --url
   Output:
   http://127.0.0.1:50192
   
   Response
   {"timestamp":"14 December, 2021 at 13:58:18","ip":"172.17.0.1"}
   
```
---
# Task 2 - Terraform: Create and Use a Module

## Create my_eks_cluster module

Create a Terraform module called "my_eks_cluster" with the following specs:

- Takes vpc ID and cluster name as input variables (others may be needed - you're the expert, right?)
- Creates EKS cluster using the VPC ID and cluster name provided by variables
- The cluster must have 2 nodes, using instance type `t3a.large`. 
- The nodes must be on the private subnets only.

Of course, you may use popular public registry modules (e.g. the eks module).

## Use my_eks_cluster module to create EKS cluster

Create a root module in terraform that creates the following resources

- A VPC with 2 public and 2 private subnets
- An EKS cluster using the 'my_eks_cluster' module you created in the previous section


As before, you may use popular public registry modules (e.g. the vpc module)

## Push your code to a public git repo
## Acceptance Criteria

Your task will be considered successful if a colleague is able to deploy infrastructure to AWS and the correct resources are created.

````sh
terraform plan
````
and
````sh
terraform apply
````

must be the only commands needed to create the EKS cluster.
